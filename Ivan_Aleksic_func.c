//Programiranje 2
//Projektni zadatak - Generator turnirskog zdrijebanja po skupinama
//Ivan Aleksic

#define _CRT_SECURE_NO_WARNINGS
#include "Ivan_Aleksic_head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int fSustavNatjecanja() {

	int pVelicinaTurnira = 0;

	do {
		printf(" Unesite sustav natjecanja (ukupni broj sudionika na turniru): ");
		scanf("%d/n", &pVelicinaTurnira);
	} while (pVelicinaTurnira != 8 && pVelicinaTurnira != 12 && pVelicinaTurnira != 16 && pVelicinaTurnira != 20 && pVelicinaTurnira != 24 && pVelicinaTurnira != 28 && pVelicinaTurnira != 32);
	
	return pVelicinaTurnira;
}
void fProvjeraDatoteke(char* datoteka, int* pBrojSudionika, int pVelicinaTurnira) {

	FILE* pDatotekaProvjera = fopen(datoteka, "rb");

	if (pDatotekaProvjera == NULL) {

		perror("\n Datoteka ne postoji, prilikom prvog pokretanja");
		pDatotekaProvjera = fopen(datoteka, "wb");

		if (pDatotekaProvjera == NULL) {

			perror("\n Datoteka se ne moze kreirati, prilikom prvog pokretanja");
			exit(EXIT_FAILURE);
		}
		else {

			fwrite(pBrojSudionika, sizeof(int), 1, pDatotekaProvjera);
			fwrite(&pVelicinaTurnira, sizeof(int), 1, pDatotekaProvjera);
			fclose(pDatotekaProvjera);
			printf(" Datoteka kreirana, prilikom prvog pokretanja.\n\n\n");
		}
	}
	else {

		fread(pBrojSudionika, sizeof(int), 1, pDatotekaProvjera);
		fread(&pVelicinaTurnira, sizeof(int), 1, pDatotekaProvjera);
		fclose(pDatotekaProvjera);
		printf("\n Datoteka postoji, prilikom prvog pokretanja.\n\n\n");
	}
}

int fGeneriranjeKoda(char* datoteka, int* pBrojSudionika, int pVelicinaTurnira) {

	FILE* pDatotekaProcitaj = NULL;
	pDatotekaProcitaj = fopen(datoteka, "rb");

	if (pDatotekaProcitaj == NULL) {

		perror(" Citanje datoteke.\n\n");
		exit(EXIT_FAILURE);
	}
	else {

		SUDIONIK* sviSudionici = NULL;

		fread(pBrojSudionika, sizeof(int), 1, pDatotekaProcitaj);

		sviSudionici = (SUDIONIK*)calloc(*pBrojSudionika, sizeof(SUDIONIK));

		if (sviSudionici == NULL) {

			perror(" Citanje svih sudionika\n\n");
			exit(EXIT_FAILURE);
		}
		else {

			fread(sviSudionici, sizeof(SUDIONIK), *pBrojSudionika, pDatotekaProcitaj);
			fclose(pDatotekaProcitaj);

			int* kodovi = (int*)calloc(*pBrojSudionika, sizeof(int));
			int i = 0;

			for (i = 0; i < *pBrojSudionika; i++) {

				*(kodovi + i) = ((sviSudionici + i)->kodSudionika);
			}

			int kod = 1 + rand() % pVelicinaTurnira;
			int j = 0;

			while (j < *pBrojSudionika) {

				if (kod == kodovi[j]) {

					kod = 1 + rand() % pVelicinaTurnira;
					j = 0;
				}
				else {

					j++;
				}
			}

			free(sviSudionici);
			return kod;
		}
	}

	return 0;
}

void fDodajSudionika(char* datoteka, int* pBrojSudionika, int pVelicinaTurnira) {
	
	FILE* pDatotekaDodajNaKraj = NULL;

	pDatotekaDodajNaKraj = fopen(datoteka, "rb+");

	if (pDatotekaDodajNaKraj == NULL) {

		perror(" Izbornik 1 - Dodavanje novog sudionika u datoteku\n\n");
		return;
	}
	else {

		SUDIONIK sudionici = { 0 };
		printf(" Unesite ime sudionika: ");
		scanf(" %30[^\n]", sudionici.ime);
		printf(" Unesite podrucje s kojeg dolazi: ");
		scanf(" %30[^\n]", sudionici.podrucje);
		printf(" Unesite poziciju unesenog sudionika na rang listi: ");
		scanf("%300s", &sudionici.pozicija);
		printf("\n\n");

		int kod = 0;
		if (*pBrojSudionika == 0) {
			kod = 1 + (float)rand() / RAND_MAX * (pVelicinaTurnira);
		}
		else {

			kod = fGeneriranjeKoda(datoteka, pBrojSudionika, pVelicinaTurnira);
		}

		sudionici.kodSudionika = kod;

		sudionici.id = (*pBrojSudionika)++;

		fseek(pDatotekaDodajNaKraj, sizeof(int) + ((*pBrojSudionika - 1) * sizeof(SUDIONIK)), SEEK_SET);
		fwrite(&sudionici, sizeof(SUDIONIK), 1, pDatotekaDodajNaKraj);
		rewind(pDatotekaDodajNaKraj);
		fwrite(pBrojSudionika, sizeof(int), 1, pDatotekaDodajNaKraj);
		fclose(pDatotekaDodajNaKraj);
	}
}

void fPretrazivanjeICitanjeSudionika(char* datoteka, int* pBrojSudionika) {

	FILE* pDatotekaProcitaj = NULL;
	pDatotekaProcitaj = fopen(datoteka, "rb");

	if (pDatotekaProcitaj == NULL) {

		perror(" Izbornik 2 - Pretrazivanje i citanje datoteke\n\n");
		exit(EXIT_FAILURE);
	}
	else {

		SUDIONIK* sviSudionici = NULL;

		fread(pBrojSudionika, sizeof(int), 1, pDatotekaProcitaj);

		if (pBrojSudionika == 0) {

			printf(" Nema unesenih sudionika!\n\n");
			fclose(pDatotekaProcitaj);
			return;
		}
		else {

			sviSudionici = (SUDIONIK*)calloc(*pBrojSudionika, sizeof(SUDIONIK));

			if (sviSudionici == NULL) {

				perror(" Citanje svih sudionika\n\n");
				exit(EXIT_FAILURE);
			}
			else {

				fread(sviSudionici, sizeof(SUDIONIK), *pBrojSudionika, pDatotekaProcitaj);
				fclose(pDatotekaProcitaj);

				int i;

				printf(" Unesite ime sudionika kojeg zelite pronaci: ");
				char privremenoIme[31] = { '\0' };
				scanf(" %30[^\n]", &privremenoIme);
				int statusPronalaska = 0;
				int indeksPronalaska = -1;

				printf("\n CITANJE SVIH UNESENIH SUDIONIKA U DATOTEKU:\n");

				for (i = 0; i < *pBrojSudionika; i++) {

					printf("\n Dodjeljeni ID sudioniku: %d\n", (sviSudionici + i)->id);
					printf(" Ime: %s\n", (sviSudionici + i)->ime);
					printf(" Podrucje: %s\n", (sviSudionici + i)->podrucje);
					printf(" Pozicija: %s\n", (sviSudionici + i)->pozicija);
					printf(" Kod: %d\n", (sviSudionici + i)->kodSudionika);
					
					if (!strcmp((sviSudionici + i)->ime, privremenoIme)) {

						statusPronalaska = 1;
						indeksPronalaska = i;
					}
				}
				if (statusPronalaska) {

					printf(" ----------------------------------------------------------------- ");
					printf("\n SUDIONIK PRONADJEN!\n");
					printf(" ID sudionika: %d\n", (sviSudionici + indeksPronalaska)->id);
					printf(" Ime sudionika: %s\n", (sviSudionici + indeksPronalaska)->ime);
					printf(" Podrucje s kojeg dolazi: %s\n", (sviSudionici + indeksPronalaska)->podrucje);
					printf(" Pozicija na rang listi: %s\n", (sviSudionici + indeksPronalaska)->pozicija);
					printf(" Kod sudionika: %d\n", (sviSudionici + indeksPronalaska)->kodSudionika);
					printf("\n\n");
				}
				else {

					printf(" ----------------------------------------------------------------- ");
					printf("\n NEPOSTOJECI SUDIONIK!\n\n\n");
				}

				free(sviSudionici);
			}
		}
	}
}

void fSortiranjeKodova(SUDIONIK* sudionici, const int n) {

	int min = -1;
	SUDIONIK temp;

	for (int i = 0; i < n - 1; i++) {

		min = i;

		for (int j = i + 1; j < n; j++) {

			if ((sudionici + min)->kodSudionika > (sudionici + j)->kodSudionika) {
				
				min = j;
			}
		}
		if (min != i) {

			temp = sudionici[i];
			sudionici[i] = sudionici[min];
			sudionici[min] = temp;
		}
	}
}

void fFormiranjeGrupa(char* datoteka, int* pBrojSudionika, int pVelicinaTurnira) {
	
	FILE* pDatotekaProcitaj = NULL;
	pDatotekaProcitaj = fopen(datoteka, "rb");

	if (pDatotekaProcitaj == NULL) {

		perror(" Citanje datoteke.\n\n");
		exit(EXIT_FAILURE);
	}
	else {

		SUDIONIK* sviSudionici = NULL;

		fread(pBrojSudionika, sizeof(int), 1, pDatotekaProcitaj);

		sviSudionici = (SUDIONIK*)calloc(*pBrojSudionika, sizeof(SUDIONIK));

		if (sviSudionici == NULL) {

			perror(" Citanje svih korisnika.\n\n");
			exit(EXIT_FAILURE);
		}
		else {

			fread(sviSudionici, sizeof(SUDIONIK), *pBrojSudionika, pDatotekaProcitaj);
			fclose(pDatotekaProcitaj);

			int n = *pBrojSudionika;
			fSortiranjeKodova(sviSudionici, n);

			int i = 0;

			FILE* grupe = fopen("skupine.txt", "w+");

			if (pVelicinaTurnira == 8) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 12) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 16) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA D\n");
				printf("\n GRUPA D\n");
				for (i = 12; i < 16; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 20) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA D\n");
				printf("\n GRUPA D\n");
				for (i = 12; i < 16; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA E\n");
				printf("\n GRUPA E\n");
				for (i = 16; i < 20; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 24) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {
					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA D\n");
				printf("\n GRUPA D\n");
				for (i = 12; i < 16; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA E\n");
				printf("\n GRUPA E\n");
				for (i = 16; i < 20; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA F\n");
				printf("\n GRUPA F\n");
				for (i = 20; i < 24; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 28) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA D\n");
				printf("\n GRUPA D\n");
				for (i = 12; i < 16; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA E\n");
				printf("\n GRUPA E\n");
				for (i = 16; i < 20; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA F\n");
				printf("\n GRUPA F\n");
				for (i = 20; i < 24; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA G\n");
				printf("\n GRUPA G\n");
				for (i = 24; i < 28; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}
			else if (pVelicinaTurnira == 32) {

				fprintf(grupe, "GRUPA A\n");
				printf(" GRUPA A\n");
				for (i = 0; i < 4; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA B\n");
				printf("\n GRUPA B\n");
				for (i = 4; i < 8; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA C\n");
				printf("\n GRUPA C\n");
				for (i = 8; i < 12; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA D\n");
				printf("\n GRUPA D\n");
				for (i = 12; i < 16; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA E\n");
				printf("\n GRUPA E\n");
				for (i = 16; i < 20; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA F\n");
				printf("\n GRUPA F\n");
				for (i = 20; i < 24; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA G\n");
				printf("\n GRUPA G\n");
				for (i = 24; i < 28; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fprintf(grupe, "\nGRUPA H\n");
				printf("\n GRUPA H\n");
				for (i = 28; i < 32; i++) {

					fprintf(grupe, "%s\n", ((sviSudionici + i)->ime));
					printf(" %s (kod: %d)\n", (sviSudionici + i)->ime, (sviSudionici + i)->kodSudionika);
				}

				fclose(grupe);
			}

			printf("\n Formiranje grupa zavrseno.\n\n\n");
			free(sviSudionici);
		}
	}
}

void fIzlazakIzPrograma(void) {

	printf(" Da li ste sigurni kako zelite zavrsiti program? ");

	char izbor[3] = { '\0' };
	scanf(" %2s", izbor);

	if (!strcmp("da", izbor)) {

		remove("sudionici.bin");
		remove("skupine.txt");
		exit(EXIT_FAILURE);
	}
	if (!strcmp("DA", izbor)) {

		remove("sudionici.bin");
		remove("skupine.txt");
		exit(EXIT_FAILURE);
	}
	if (!strcmp("Da", izbor)) {

		remove("sudionici.bin");
		remove("skupine.txt");
		exit(EXIT_FAILURE);
	}
}

void fIzbornik(char* datoteka, int* pBrojSudionika, int pVelicinaTurnira) {

	int izbornik = -1;

	while (1) {

		system("pause");
		system("cls");

		printf("\n \
                \n ----------------------------------------------------------------- \
                \n --------> GENERATOR TURNIRSKOG ZDRIJEBANJA PO SKUPINAMA <-------- \
                \n ----------------------------------------------------------------- \
                \n \
				\n  1. Dodavanje novih sudionika u datoteku sudionici.bin            \
                \n \
				\n  2. Pretrazivanje i citanje sudionika iz datoteke sudionici.bin   \
                \n \
				\n  3. Zavrsetak programa                                            \
			    \n \
                \n ----------------------------------------------------------------- \
				\n \n");

		printf("\n Odaberite radnju iz izbornika: ");
		scanf("%d", &izbornik);
		printf("\n\n");
		
		switch (izbornik) {

		case 1:

			system("pause");
			system("cls");

			printf("\n Trenutni broj unesenih sudionika u datoteci je %d, ukupni broj sudionika na turniru je %d.\n", *pBrojSudionika, pVelicinaTurnira);
			printf("\n Konacni raspored po skupinama biti ce napravljen za prvih %d sudionika koji su upisani u datoteku.\n\n\n", pVelicinaTurnira);

			if (*pBrojSudionika < pVelicinaTurnira) {

				fDodajSudionika(datoteka, pBrojSudionika, pVelicinaTurnira);
			}
			else {

				printf("\n Dodali ste dovoljan broj sudionika!\n");
				printf("\n Formiranje grupa u tijeku...\n\n");
				fFormiranjeGrupa(datoteka, pBrojSudionika, pVelicinaTurnira);
			}
			break;
		case 2:

			system("pause");
			system("cls");

			fPretrazivanjeICitanjeSudionika(datoteka, pBrojSudionika);
			break;
		case 3:

			system("pause");
			system("cls");

			fIzlazakIzPrograma();
			break;
		default:
			printf("\n Krivo odabrana opcija, pokusajte ponovno.\n\n\n");
		}
	}
}
