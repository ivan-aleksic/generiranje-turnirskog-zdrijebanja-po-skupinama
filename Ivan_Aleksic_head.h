//Programiranje 2
//Projektni zadatak - Generator turnirskog zdrijebanja po skupinama
//Ivan Aleksic
//#pragma once

#ifndef HEADER_H
#define HEADER_H

typedef struct sudionik {

	int id;
	int kodSudionika;
	char ime[31];
	char podrucje[31];
	char pozicija[151];
} SUDIONIK;

int fSustavNatjecanja();
void fProvjeraDatoteke(char*, int*, int);
int fGeneriranjeKoda(char*, int*, int);
void fDodajSudionika(char*, int*, int);
void fPretrazivanjeICitanjeSudionika(char*, int*);
void fSortiranjeKodova(SUDIONIK*, const int);
void fFormiranjeGrupa(char*, int*, int);
void fIzlazakIzPrograma(void);
void fIzbornik(char*, int*, int);

#endif //HEADER_H