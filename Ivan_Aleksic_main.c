//Programiranje 2
//Projektni zadatak - Generator turnirskog zdrijebanja po skupinama
//Ivan Aleksic

#define _CRT_SECURE_NO_WARNINGS
#include "Ivan_Aleksic_head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {

	char* sudionici = "sudionici.bin";
	int brojSudionika = 0;
	int sudionik = 0;
	int velicinaTurnira = 0;

	srand((unsigned)time(NULL));

	velicinaTurnira = fSustavNatjecanja();
	fProvjeraDatoteke(sudionici, &brojSudionika, velicinaTurnira);
	fIzbornik(sudionici, &brojSudionika, velicinaTurnira);

	return 0;
}